resource "google_storage_bucket" "auto-expire" {
  name          = "pers-terr1"
  location      = "us-west1"
  force_destroy = true

  
# Life cycle policy for deleteing bucket 
  lifecycle_rule {
    condition {
      age = 3                           
    }
    action {
      type = "Delete"
    }
  }
}
